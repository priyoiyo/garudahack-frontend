import React from 'react';
import { Row, Col, Divider } from 'antd';
import { YoutubeFilled, HomeFilled, VideoCameraFilled } from '@ant-design/icons';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import LaunchLive from '../../Component/Teacher/LaunchLive';
import Offline from '../../Component/Teacher/Offline';
import Publish from '../../Component/Teacher/Publish';

const TeacherPage = () => {
    return (
        <div>
            <Divider />
            <TeachersMenu />
            <Divider />
            <h3 className="st-container">Hello, Teacher.</h3>
            <br></br>
            <Router>
                <Switch>
                    <Route path='/teacher/live' component={LaunchLive} />
                    <Route path='/teacher/publish' component={Publish} />
                    <Route path='/teacher/offline' component={Offline} />
                </Switch>
            </Router>
        </div>
    )
}

class TeachersMenu extends React.Component {

    render() {
        return (
            <div mode="horizontal">
                <Row type="flex" justify="center">
                    <a href="/teacher/live" className="Link-TS">
                        <Col span={6}>
                            <VideoCameraFilled className="Icon-TS" />
                            <p key="live">
                                Live Course
                        </p>
                        </Col>
                    </a>
                    <a href="/teacher/publish" className="Link-TS">
                        <Col span={6}>
                            <YoutubeFilled className="Icon-TS" />
                            <p key="materials">
                                Publish Lesson
                        </p>
                        </Col>
                    </a>
                    <a href="/teacher/offline" className="Link-TS">
                        <Col span={6}>
                            <HomeFilled className="Icon-TS" />
                            <p key="offline">
                                Offline Session
                        </p>
                        </Col>
                    </a>
                </Row>
            </div >
        );
    }
}


export default TeacherPage;