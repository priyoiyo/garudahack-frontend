import React, { useState, useEffect } from 'react';
import './lesson.css';
import VideoOverview from '../../Component/VideoOverview';
import { Row, Col, Rate, Card, Button, Carousel, Comment, Avatar  } from 'antd';
import { FieldTimeOutlined, ShoppingCartOutlined } from '@ant-design/icons';

const LessonDetail = (props) => {

    const [ratingScore] = useState("4.4");
    const [ratingTotal] = useState("14");
    const [studentTotal] = useState("100");
    const [teacherName] = useState("Bayu Prihandika");
    const [lessonCategorize] = useState("Online Learning");
    const [lessonSchedule] = useState("Nov 02 - 11 | From 10am to 6pm");
    const jsonReview = [{
        id: 1,
        muridName: "Joko Wibowo",
        comment: "mantap berkualitas, produk original"
    }, {
        id: 2,
        muridName: "Agus Gumelar",
        comment: "apa aja yang mau aku tulis, suka-suka aku. aku pengen nulis panjang lebar komentar ini. semoga bermanfaat"
    },{
        id: 3,
        muridName: "Purnama Farokhi",
        comment: "mantap berkualitas, produk original"
    }, {
        id: 4,
        muridName: "Angelia Rosalia",
        comment: "lihatlah kemari ftk telah datang. di kompetisi kita harus menang. segala rintangan akan kami terjang. untuk fakultas kami ftk harus menang"
    }]
    const [lessonReview] = useState (jsonReview);
    const jsonLessonRecommend = [{
      id: 1,
      lessonName: "Matematika Dasar Kelas 5 SD",
      description: "mantap berkualitas, produk original"
  }, {
      id: 2,
      lessonName: "Ilmu Pengetahuan Alam",
      description: "apa aja yang mau aku tulis, suka-suka aku. aku pengen nulis panjang lebar komentar ini. semoga bermanfaat"
  },{
      id: 3,
      lessonName: "Algebra The Conspiracy Theory",
      description: "mantap berkualitas, produk original"
  }, {
      id: 4,
      lessonName: "Pelajaran Peta Buta Seri pengetahuan global",
      description: "lihatlah kemari ftk telah datang. di kompetisi kita harus menang. segala rintangan akan kami terjang. untuk fakultas kami ftk harus menang"
  }]
  const [lessonRecommend] = useState (jsonLessonRecommend);
  const [lessonById, setLessonById] = useState([])
  const [videoById, setVideoById] = useState([])


	useEffect(() => {
		fetch(`https://garudahack-api.herokuapp.com/api/v1/course/find/${props.match.params.id}`, {
			method: 'GET'
		})
		.then(response => response.json())
    .then(result => {
      setLessonById(result.result);
      let data = result.result;
      let dataMedia = data.media;
      setVideoById(dataMedia.secure_url);
    })
  }, []); //eslint-disable-line
  

  return (
    <div>


        <div className="std-margin">

        <Row style={{backgroundColor:"whitesmoke"}}>
            <Col  xs={{span:24}} sm={{span:11, offset:1}} style={{maxWidth:"900px", marginTop:"20px", marginBottom:"30px"}}>
              
                <VideoOverview url={videoById}/>
                <h1 className="lesson-title" >{lessonById.title}</h1>
                <section>
                    <span> {ratingScore}  </span>
                    <Rate allowHalf defaultValue={4.5} disabled className="rating-star" />
                    <span>({ratingTotal} ratings) {studentTotal} students</span>
                    <br/>
                    <FieldTimeOutlined /><span> {lessonSchedule}</span>
                    <br/>
                    <span>Categorized {lessonCategorize}</span>
                    <br/>
                    
                    <span>Created by <a href="/lesson-detail">{teacherName}</a></span>
                    <br/>
                    <span><h2 style={{textDecoration:"line-through", display:"inline-block"}}>Rp 50.000</h2> <h3 style={{display:"inline-block"}}>Rp 10.000</h3> </span>
                    <span style={{display:"block", marginTop:"0px", lineHeight:"0"}}>discount 80%</span>
                </section>
            </Col>
            <Col xs={{span:24}} sm={{span:10, offset:1}} className="ld-column2">
                <Card title="Description" className="card-box">
                    <span style={{textAlign:"justify"}}>{lessonById.description}</span>
                </Card>
                <Button type="primary"  href="/lesson-checkout" className="btn-ld">
                    <ShoppingCartOutlined /> Order Lesson
                </Button>

            </Col>
        </Row>
        <Row>
            <Col xs={{span:24}} sm={{span:12}} style={{marginTop:"20px"}}>

  <Carousel autoplay dotPosition={"bottom"} className="ld-carousel">
          {lessonReview.map(review => {
            return (
              <div key={review.id}>
                  <Comment
                        style={{margin:"10px 20px 20px 20px"}}
                        author={<a href="/lesson-detail">{review.muridName}</a>}
                        avatar={
                          <Avatar
                            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                            alt="Han Solo"
                          />
                        }
                        content={
                          <span>
                            <span> {ratingScore}  </span>
                            <Rate allowHalf defaultValue={4.5} disabled className="rating-star" />
                            <br/>
                            {review.comment}
                          </span>
                        
                        }
                      />
            </div>
          )}
            )
          }

  </Carousel>
</Col>
<Col xs={{span:24}} sm={{span:10, offset:1}} style={{marginTop:"20px"}}>
        <Card title="Lesson Recommend For You" className="card-box">
        {lessonRecommend.map(lesson => {
                    return (
                      <div key={lesson.id}>
                        <Row>
                        <Col xs={12} sm={6} >
                            <img alt="imagee" style={{width:"100px", marginBottom:"20px"}} src="https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png"/>
                          </Col>
                          <Col xs={12} sm={18}>
                          <h4 style={{marginTop:"10px"}}>{lesson.lessonName}</h4>  
                          </Col>
                        </Row>
                
                    </div>
                  )}
                    )
                  }
        </Card>
            <br />

            
            </Col>
        </Row>


  </div>
        

        
    </div>
  );
}

export default LessonDetail;