import React, { useState } from 'react';
import './lesson.css';
import { Row, Col, Input, Card, Button, Tabs } from 'antd';
import { ShoppingCartOutlined, TagOutlined } from '@ant-design/icons';
import logoOvo from '../../Asset/image/ovologo.png';
import logoGopay from '../../Asset/image/gopaylogo.jpg';
import logoBni from '../../Asset/image/logobni2.jpeg'



function LessonCheckout() {

  const jsonLessonCheckout = {
    id: 1,
    lessonName: "Matematika Dasar Kelas 5 SD",
    price: 10000
}
const [lessonCheckoutCart] = useState (jsonLessonCheckout);



const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

  return (
    <div>
        <Row style={{marginTop:"20px"}}>
            <Col xs={24} sm={{span:13, offset:1}}>
            <Card title="Payment Option" style={{textAlign:"left"}}>
            <Tabs defaultActiveKey="1" onChange={callback}>
    <TabPane tab="Ovo" key="1">
      <h2>Ovo Transfer Payment</h2>
      <img style={{width:"200px"}} src={logoOvo} alt="logo" />
      <h3>Instruksi</h3>
    <p>1. Buka Aplikasi OVO</p>
    <p>2. Pilih Menu Transfer</p>
    <p>3. Masukkan Nomor Tujuan 081321341232</p>
    <p>4. Input Nominal Sebesar Rp {lessonCheckoutCart.price}</p>
    <p>5. Transfer</p>
    </TabPane>
    <TabPane tab="Gopay" key="2">
    <h2>Ovo Transfer Payment</h2>
    <img style={{width:"200px"}}  src={logoGopay} alt="logo" />
    <h3>Instruksi</h3>
    <p>1. Buka Aplikasi Gojek</p>
    <p>2. Pilih Menu Transfer</p>
    <p>3. Masukkan Nomor Tujuan 081321341232</p>
    <p>4. Input Nominal Sebesar Rp {lessonCheckoutCart.price}</p>
    <p>5. Transfer</p>
    </TabPane>
    <TabPane tab="Transfer" key="3">
    <h2>Bank Transfer Payment</h2>
    <img style={{width:"200px"}}  src={logoBni} alt="logo" />
    <h3>Instruksi</h3>
    <p>1. Buka Aplikasi Mobile Banking/ Masukkan ATM</p>
    <p>2. Pilih Menu Transfer ke Bank BNI</p>
    <p>3. Masukkan rekening 8787872631 a.n Ananda Budiati </p>
    <p>4. Input Nominal Sebesar Rp {lessonCheckoutCart.price}</p>
    <p>5. Transfer</p>

    </TabPane>
  </Tabs>   </Card>

            </Col>
            <Col xs={24} sm={{span:8, offset:1}}>
            <Card style={{textAlign:"left"}} title="Use Voucher" >
                <span>
                <Input size="large" placeholder="input promo code" prefix={<TagOutlined />} /></span>

                </Card>
            <Card style={{textAlign:"left"}} title="Summary checkout">
                    <Row>
                        <Col xs={{span:24}} sm={{span:14}}>
                            <h4>{lessonCheckoutCart.lessonName} | </h4>
                        </Col>
                        <Col xs={{span:24}} sm={{span:10}} style={{position:"absolute", right:"20px"}}>
                            <h4 >Rp {lessonCheckoutCart.price}</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{span:24}} sm={{span:14}}>
                            <h4>Voucher -</h4>
                        </Col>
                        <Col xs={{span:24}} sm={{span:10}} style={{position:"absolute", right:"20px"}}>
                            <h4 >- Rp 0</h4>
                        </Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col xs={{span:24}} sm={{span:14}}>
                            <h4>Total -</h4>
                        </Col>
                        <Col xs={{span:24}} sm={{span:10}} style={{position:"absolute", right:"20px"}}>
                            <h4 >Rp {lessonCheckoutCart.price}</h4>
                        </Col>
                    </Row>
                </Card>
                <Button type="primary"   className="btn-ld">
                    <ShoppingCartOutlined /> Payment Confirmation
                </Button>
            </Col>
        </Row>
        
    </div>
  );
}

export default LessonCheckout;