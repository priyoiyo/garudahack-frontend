import React from 'react';
import { Row, Col, Divider } from 'antd';
import { ReadOutlined, HomeFilled, VideoCameraFilled } from '@ant-design/icons';
import {
    BrowserRouter as Router,
    Switch,
    Route,

} from 'react-router-dom';
import DiscoverLive from '../../Component/Student/DiscoverLive';
import DiscoverMaterials from '../../Component/Student/DiscoverMaterials';
import DiscoverOffline from '../../Component/Student/DiscoverOffline';

const StudentPage = () => {
    return (
        <div>
            <Divider />
            <StudentsMenu />
            <Divider />
            <h3 className="st-container">Hello, Student.</h3>
            <br></br>
            <Router>
                <Switch>
                    <Route path='/student/live' component={DiscoverLive} />
                    <Route path='/student/materials' component={DiscoverMaterials} />
                    <Route path='/student/live' component={DiscoverLive} />
                    <Route path='/student/offline' component={DiscoverOffline} />
                </Switch>
            </Router>
        </div>
    )
}

class StudentsMenu extends React.Component {

    render() {
        return (
            <div mode="horizontal">
                <Row type="flex" justify="center">
                    <a href="/student/live" className="Link-TS">
                        <Col span={6}>
                            <VideoCameraFilled className="Icon-TS" />
                            <div>
                                Live Course
                            </div>

                        </Col>
                    </a>
                    <a href="/student/materials" className="Link-TS">
                        <Col span={6}>
                            <ReadOutlined className="Icon-TS" />
                            <div>
                                Find Materials
                        </div>
                        </Col>
                    </a>
                    <a href="/student/offline" className="Link-TS">
                        <Col span={6}>
                            <HomeFilled className="Icon-TS" />
                            <div>
                                Offline Session
                        </div>
                        </Col>
                    </a>
                </Row>
            </div >
        );
    }
}


export default StudentPage;