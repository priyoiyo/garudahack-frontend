import React from 'react';
import './landing.css'


function LandingPage() {


  return (
    <div>
      <div className="showcase">
    <div className="content">
      {/* <img src={"https://image.ibb.co/ims4Ep/logo.png"} className="logo" alt="logo"/> */}
      <div className="title">
        Garudacation
      </div>
      <div className="text">
        learning and teaching more easier and so fun!
      </div>
    </div>
  </div>

 
  <section className="services">
    <div className="container grid-3 center">
      <div>
        <i className="fab fa-youtube fa-3x"></i>
        <h3>Find Nearby Teacher</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, reiciendis!</p>
      </div>
      <div>
        <i className="fas fa-chalkboard-teacher fa-3x"></i>
        <h3>Live Courses</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, reiciendis!</p>
      </div>
      <div>
        <i className="fas fa-briefcase fa-3x"></i>
        <h3>Unlimited Time</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, reiciendis!</p>
      </div>
    </div>
  </section>


  <section className="about bg-light">
    <div className="container">
      <div className="grid-2">
        <div className="center">
          <i className="fas fa-laptop-code fa-10x"></i>
        </div>
        <div>
          <h3>About Us</h3>
          <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Non eos aperiam labore consectetur maiores ea magni exercitationem
            similique laborum sed, unde, autem vel iure doloribus aliquid. Aspernatur explicabo consectetur consequatur non
            nesciunt debitis quos alias soluta, ratione, ipsa officia reiciendis.</p>
        </div>
      </div>
    </div>
  </section>

  <div className="center bg-dark">
    <p>Garudacation&copy; 2020</p>
  </div>
    </div>

  );
}

export default LandingPage;