import React from 'react';
import axios from 'axios';
import { Upload, Button, Input, Divider } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { UploadOutlined } from '@ant-design/icons';

export default class Publish extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            mentor: '5f373967791d40000419279d',
            title: '',
            description: ''
        }

        this.onChangeMentor = this.onChangeMentor.bind(this)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangeDescription = this.onChangeDescription.bind(this)
        this.submitLesson = this.submitLesson.bind(this)
    }
    onChangeMentor(event) {
        this.setState({
            mentor: event.target.value
        })
    }
    onChangeTitle(event) {
        this.setState({
            title: event.target.value
        })
    }
    onChangeDescription(event) {
        this.setState({
            description: event.target.value
        })
    }
    submitLesson(event) {
        event.preventDefault()
        fetch(`https://garudahack-api.herokuapp.com/api/v1/course/store`, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'authentication-token': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjM4MDlmMzNkZDUwMTAwMDQzZDA2ZmIiLCJ1c2VybmFtZSI6ImFuaSBhamphaCIsImVtYWlsIjoiYW5pQHlvcG1haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkb3A3TEt6MnJweVlHc1N0VEhucUI3dXUvMDNzcy4yRld4a0VPS3dvRlNuZXpCOEhBY3ZYTXEiLCJ1c2VyX3R5cGUiOiIxIiwiaWF0IjoxNTk3NTE1MzkzLCJleHAiOjE1OTc1MjI1OTN9.w3j5IX827njy0pzjE6tyKl717Dl0U3EgbQJMTtnPYfA'
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                mentor: this.state.mentor,
                title: this.state.title,
                description: this.state.description
            })
        }).then(response => console.log(response.json()))
            .catch((error => console.log(error)))

        this.setState({
            title: '',
            description: ''
        })
    }


    render() {
        return (
            <div className='st-container' >
                <h2>Publish a Lesson</h2>
                <Input style={{ marginBottom: '1rem' }} placeholder="Title" value={this.state.title} onChange={this.onChangeTitle} />
                <TextArea style={{ marginBottom: '1rem' }} placeholder="Description..." value={this.state.description} onChange={this.onChangeDescription} />
                <Button onClick={this.submitLesson} type="primary" className="btn-ld">
                    Submit
                </Button>
                <Divider />
                <Upload {...props}>
                    <Button>
                        <UploadOutlined /> Upload
                    </Button>
                </Upload>
            </div>
        )
    }

}

const props = {
    // action: 'https://garudahack-api.herokuapp.com/api/v1/course/store',
    listType: 'picture',
    previewFile(file) {
        console.log('Your upload file:', file);
        // Your process logic. Here we just mock to the same file
        return fetch('https://garudahack-api.herokuapp.com/api/v1/course/upload/5f3809f33dd50100043d0703', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'authentication-token': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjM4MDlmMzNkZDUwMTAwMDQzZDA2ZmIiLCJ1c2VybmFtZSI6ImFuaSBhamphaCIsImVtYWlsIjoiYW5pQHlvcG1haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkb3A3TEt6MnJweVlHc1N0VEhucUI3dXUvMDNzcy4yRld4a0VPS3dvRlNuZXpCOEhBY3ZYTXEiLCJ1c2VyX3R5cGUiOiIxIiwiaWF0IjoxNTk3NTE1MzkzLCJleHAiOjE1OTc1MjI1OTN9.w3j5IX827njy0pzjE6tyKl717Dl0U3EgbQJMTtnPYfA'
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: {
                image: file
            }
        })
            .then(res => res.json())
            .then(({ thumbnail }) => thumbnail);
    },
};