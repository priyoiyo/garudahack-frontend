import React from 'react';
import { Input, Button } from 'antd';
import TextArea from 'antd/lib/input/TextArea';

const LaunchLive = () => {
    return (
        <div className='st-container'>
            <h2>Teach a Course Live</h2>
            <Input style={{ marginBottom: '1rem' }} placeholder="Topic" />
            <Input style={{ marginBottom: '1rem' }} placeholder="Meeting url" />
            <Input style={{ marginBottom: '1rem' }} placeholder="Date & Time" />
            <TextArea style={{ marginBottom: '1rem' }} placeholder="Description..." />
            <Button type="primary" className="btn-ld">
                Submit
            </Button>
        </div>
    )
}

export default LaunchLive;