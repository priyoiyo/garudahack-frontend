import React from 'react';
import { List, Avatar } from 'antd';

const Offline = () => {
    return (
        <div className='st-container'>
            <h2>Appointment Request</h2>
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={item => (
                    <List.Item  style={{ display: 'flex' }}>
                        <List.Item.Meta
                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<a href="https://ant.design">{item.title}</a>}
                            description="Hi Sir. I am writing to inform you that I am interested to make an appointment..."
                        />
                    </List.Item>
                )}
            />
        </div>
    )
}

const data = [
    {
        title: 'Momo Taro',
    },
    {
        title: 'Siti Zubaedah',
    },
    {
        title: 'Indra Bekti',
    },
    {
        title: 'Soka',
    },
];

export default Offline;