import React, { useState, useEffect } from 'react';
import {useHistory} from 'react-router-dom'
import 'antd/dist/antd.css';
import { Row, Col, InputNumber, Select, Layout, Menu, Button, Input, Drawer, Modal, Space } from 'antd';
import {
    UserOutlined, LaptopOutlined,
    NotificationOutlined, BarsOutlined, MailOutlined,
    AppstoreOutlined, SettingOutlined,
    LogoutOutlined, LoginOutlined,
    EyeInvisibleOutlined, EyeTwoTone
} from '@ant-design/icons';
import DistanceHelper from '../../helper/distance';
// import ModalLogin from '../login/ModalLogin';
// import ModalSignUp from '../login/ModalSignUp'

const { Search } = Input;
const { SubMenu } = Menu;

const SidebarMenu = (props) => {

    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [entity, setEntity] = useState('mentor')
    const [isHidden, setIsHidden] = useState(true)
    const [current, setCurrent] = useState('app')
    const [visibleLogin,setVisibleLogin] = useState(false)
    const [visibleSignUp,setVisibleSignUp] = useState(false)
    const [emailRegis, setEmailRegis] = useState('')
    const [usernameRegis, setUserNameRegis] = useState('')
    const [passwordRegis, setPasswordRegis] = useState('')
    const [userRoleRegis, setUserRoleRegis] = useState(0)
    const [emailLogin, setEmailLogin] = useState('')
    const [passwordLogin, setPasswordLogin] = useState('')
    const history = useHistory();
    const { Option } = Select;
    const showModalLogin = () => {
        setVisibleLogin(true);
    };
  
    const handleOkLogin = e => {
      console.log(e);
      setVisibleLogin(false);
      history.push("/teacher")
      window.location.reload();

    };
  
    const handleCancelLogin = e => {
      console.log(e);
      setVisibleLogin(false);
    };
    const showModalSignUp = () => {
        setVisibleSignUp(true)
    };
    // const handleInput = event => {
    //     setUserRoleRegis(event.target.value)    

    // }
    const handleOkSignUp = e => {
      setVisibleSignUp(false);
      if(emailRegis.length && passwordRegis.length) {
        console.log("input verified")
        const payload={
            "email":emailRegis,
            "password":passwordRegis,
        }
        fetch(`https://garudahack-api.herokuapp.com/api/v1/user/`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({email: emailRegis, password: passwordRegis, username:usernameRegis, user_type:1})
            })
            .then(response => response.json())
        .then(result => {
            alert(result.status)
        })
    }
    };
  
    const handleCancelSignUp = e => {
      console.log(e);
      setVisibleSignUp(false);
    };

    useEffect(() => {
        (sessionStorage.getItem('isLoggedIn')) ? setIsLoggedIn(true) : setIsLoggedIn(false)
    }, [])
    function handleClick(e) {
        console.log('click ', e);
        setCurrent()
    };
    let sidebar;

    if (entity === 'mentor') {
        sidebar = (
            <Layout>
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%', borderRight: 0 }}>
                    <Menu.Item key="sub1" icon={<UserOutlined />}>
                        {(isHidden) ? '' : 'User Account'}
                    </Menu.Item>
                    <SubMenu key="sub2" icon={<LaptopOutlined />} title={(isHidden) ? '' : 'Nav'}>
                        <Menu.Item key="5">option5</Menu.Item>
                        <Menu.Item key="6">option6</Menu.Item>
                        <Menu.Item key="7">option7</Menu.Item>
                        <Menu.Item key="8">option8</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub3" icon={<NotificationOutlined />} title={(isHidden) ? '' : 'Nav'}>
                        <Menu.Item key="9">option9</Menu.Item>
                        <Menu.Item key="10">option10</Menu.Item>
                        <Menu.Item key="11">option11</Menu.Item>
                        <Menu.Item key="12">option12</Menu.Item>
                    </SubMenu>
                </Menu>
            </Layout>
        )
    } else if (entity === 'student') {
        sidebar = (
            <Layout>
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%', borderRight: 0 }}>
                    <Menu.Item key="sub1" icon={<UserOutlined />}>
                        {(isHidden) ? '' : 'User Account'}
                    </Menu.Item>
                    <SubMenu key="sub2" icon={<LaptopOutlined />} title={(isHidden) ? '' : 'Nav'}>
                        <Menu.Item key="5">option5</Menu.Item>
                        <Menu.Item key="6">option6</Menu.Item>
                        <Menu.Item key="7">option7</Menu.Item>
                        <Menu.Item key="8">option8</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub3" icon={<NotificationOutlined />} title={(isHidden) ? '' : 'Nav'}>
                        <Menu.Item key="9">option9</Menu.Item>
                        <Menu.Item key="10">option10</Menu.Item>
                        <Menu.Item key="11">option11</Menu.Item>
                        <Menu.Item key="12">option12</Menu.Item>
                    </SubMenu>
                </Menu>
            </Layout>
        )
    }
    let header;
    if (isLoggedIn) {
        header = (
            <Menu onClick={handleClick} disabled selectedKeys={[current]} mode="horizontal">
                <Menu.Item>
                    <BarsOutlined style={{ fontSize: '20px' }} onClick={() => setIsHidden(!isHidden)} />
                </Menu.Item>
                <Menu.Item key="mail">
                    <a href='/'>GARUDACATION</a>
                </Menu.Item>
                <SubMenu style={{ float: "right" }} icon={<UserOutlined />} title="Account">
                    <Menu.ItemGroup title="Profile">
                        <Menu.Item key="profile" icon={<UserOutlined />}>Profile</Menu.Item>
                        <Menu.Item key="setting" icon={<SettingOutlined />}>Settings</Menu.Item>
                        <Menu.Item key="logout" icon={<LogoutOutlined />}>Logout</Menu.Item>
                    </Menu.ItemGroup>
                </SubMenu>
                <Menu.Item style={{ float: 'right' }} key="notifications" icon={<MailOutlined />}>Notifications</Menu.Item>
                <Menu.Item style={{ float: 'right' }} key="dashboard" icon={<AppstoreOutlined />}>
                    <a href="/dashboard" >Dashoard</a>
                </Menu.Item>
            </Menu>

        )
    } else {
        header = (

            <Menu onClick={handleClick} disabled selectedKeys={[current]} mode="horizontal">
                <Menu.Item>
                    <BarsOutlined style={{ fontSize: '20px' }} onClick={() => setIsHidden(!isHidden)} />
                </Menu.Item>
                <Menu.Item key="mail">
                    <a href='/'><strong>GARUDACATION</strong></a>
                </Menu.Item>
                <Menu.Item style={{ float: "right" }} title="Account">
                    <Button onClick={showModalSignUp} type="primary" shape="round" icon={<UserOutlined />}  > Sign Up </Button>
                    <Button onClick={showModalLogin} shape="round" icon={<LoginOutlined />}  > Log In </Button>
                </Menu.Item>
                <Menu.Item style={{ float: 'right' }}>
                    <Search placeholder="input search loading with enterButton" loading={false} />
                </Menu.Item>

            </Menu>

        )
    }
    return (
        <>
            {header}
            <Drawer
                placement={'left'}
                closable={false}
                onClose={() => setIsHidden(true)}
                visible={!isHidden}
                key={'right'}>

                <Menu mode='horizontal'>
                    <BarsOutlined style={{ fontSize: '16px' }} onClick={() => setIsHidden(!isHidden)} />
                    <Menu.Item><a href='/'><strong>GARUDACATION</strong></a></Menu.Item>
                </Menu>
                {sidebar}
            </Drawer>

            <Modal
            title="Login"
            visible={visibleLogin}
            onOk={handleOkLogin}
            onCancel={handleCancelLogin}
          >
            <Input placeholder="Email" />
            <Input.Password
            placeholder="Password"
            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            />

          </Modal>
          <Modal
            title="SignUp"
            visible={visibleSignUp}
            onOk={handleOkSignUp}
            onCancel={handleCancelSignUp}
          >
            <Input placeholder="Username" onChange={e => setUserNameRegis(e.target.value)} />
            <Input placeholder="Email" onChange={e => setEmailRegis(e.target.value)} />
            <Input.Password
            placeholder="Password"
            onChange={e => setPasswordRegis(e.target.value)}
            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            />
            
            <Select defaultValue="Role User" >
                <Option value={1}>Mentor</Option>
                <Option value={2}>Murid</Option>
            </Select>

          </Modal>

        </>

    )
}
export default SidebarMenu
