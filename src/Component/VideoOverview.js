import React from "react"
import ReactPlayer from "react-player"
import "./responsive-player.css"

const VideoOverview = props => {
      return (
        <div className='player-wrapper'>
          <ReactPlayer
            className='react-player'
            url={props.url}
            width='100%'
            height='100%'
            controls={true}
            max-width='300px'
          />
        </div>
      )
  }

export default VideoOverview