import React, {useState} from 'react'
import { Modal, Button } from 'antd';



const ModalLogin = props => {
    const [visible,setVisible] = useState(false)
  
    const showModal = () => {
        setVisible(true)
    };
  
    const handleOk = e => {
      console.log(e);
      setVisible(false);
    };
  
    const handleCancel = e => {
      console.log(e);
      setVisible(false);
    };
      return (
        <div>
            <Button type="primary" onClick={showModal}>
            Login
            </Button>
          <Modal
            title="Basic Modal"
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Modal>
        </div>
      )
  }

  export default ModalLogin