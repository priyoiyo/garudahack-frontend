import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature } from 'react-mapbox-gl';
import { Button, Input, Layout, Divider } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
const { Sider, Content } = Layout;

class DiscoverOffline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            lat: 0,
            long: 0
        };
    }

    componentDidMount() {
        // Get location of user
        const success = position => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;
            console.log(latitude, longitude);
            this.setState({
                lat: latitude,
                long: longitude
            });
        };

        const error = () => {
            console.log("Unable to retrieve your location");
        };

        navigator.geolocation.getCurrentPosition(success, error);

        fetch("https://garudahack-api.herokuapp.com/api/v1/location/store")
            .then(res => res.json())
            .then(parsedJSON => parsedJSON.result.map(data => (
                {
                    key: `${data.createdAt}`,
                    id: `${data.mentor_id._id}`,
                    name: `${data.mentor_id.name}`,
                    long: `${data.location.coordinates[0]}`,
                    lat: `${data.location.coordinates[1]}`
                }

            )))
            .then(items => this.setState({
                items,
                isLoaded: false
            }))
            .catch(error => console.log('parsing failed', error))
    }

    render() {
        const { items } = this.state;
        return (
            <div className='st-container'>
                <Layout>
                    <Content>
                        <Map
                            style="mapbox://styles/mapbox/streets-v9"
                            containerStyle={{
                                height: '100vh',
                                width: '50vw'
                            }}
                            center={[this.state.long, this.state.lat]}
                            zoom={[13]}
                        >
                            <Layer type="symbol"
                                layout={{
                                    "icon-image": "harbor-15",
                                    "icon-allow-overlap": true,
                                    "text-field": "You are here",
                                    "text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
                                    "text-size": 11,
                                    "text-transform": "uppercase",
                                    "text-letter-spacing": 0.05,
                                    "text-offset": [0, 1.5]
                                }}>
                                <Feature coordinates={[this.state.long, this.state.lat]} />
                            </Layer>
                            <div>

                                {
                                    items.length > 0 ? items.map(item => {
                                        const { key, name, lat, long } = item;
                                        return (

                                            <Layer key={key}
                                                type="symbol"
                                                layout={{
                                                    "icon-image": "harbor-15",
                                                    "icon-allow-overlap": true,
                                                    "text-field": `${name}`,
                                                    "text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
                                                    "text-size": 11,
                                                    "text-transform": "uppercase",
                                                    "text-letter-spacing": 0.05,
                                                    "text-offset": [0, 1.5]
                                                }}>
                                                <Feature coordinates={[long, lat]} />
                                            </Layer>
                                        );
                                    }) : null
                                }
                            </div>
                        </Map>;
                    </Content>
                    <Sider style={{ backgroundColor: 'whitesmoke' }}>
                        <p style={{ textAlign: 'center' }}>Schedule a meeting</p>
                        <Divider />
                        <Input placeholder="Pick a date" style={{ marginBottom: '1rem' }} />
                        <Input placeholder="Choose teacher" style={{ marginBottom: '1rem' }} />
                        <TextArea placeholder="Enter message..." style={{ marginBottom: '1rem' }} />
                        <Button type="primary" className="btn-ld">
                            Submit
                        </Button>
                        <Divider />
                    </Sider>
                </Layout>
            </div>
        )
    }
}

const Map = ReactMapboxGl({
    accessToken:
        'pk.eyJ1IjoiY2F0cmVlZGxlIiwiYSI6ImNrMGs4dzhlcDBpem4zbHFka2p2Ym15amQifQ.9eJuOs0nYJbg5SL3oveelw'
});

export default DiscoverOffline;