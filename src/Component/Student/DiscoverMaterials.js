import React, { Component } from 'react';
import { Input, List, Avatar, Space, Divider } from 'antd';

import { MessageOutlined, LikeOutlined, StarOutlined } from '@ant-design/icons';

class DiscoverMaterials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: []
        }
    }
    componentDidMount() {
        fetch('https://garudahack-api.herokuapp.com/api/v1/course/store')
            .then(res => res.json())
            .then(parsedJSON => parsedJSON.result.map(data => (
                {
                    href: `/lesson-detail/${data._id}`,
                    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
                    mentor: `${data.mentor.name}`,
                    title: `${data.title}`,
                    description: `${data.description}`,
                    src: `${data.media.secure_url}`,
                    content: 'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.'
                }
            )))
            .then(listData => this.setState({
                listData,
                isLoaded: false
            }))
            .catch(error => console.log('parsing failed', error))
    }
    render() {
        const { listData } = this.state;
        return (
            <div className='st-container'>
                <Input placeholder="find materials..." />
                <Divider />
                <h2>Available Lessons</h2>
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 3,
                    }}
                    dataSource={listData}

                    renderItem={item => (
                        <List.Item
                            key={item.title}
                            actions={[
                                <IconText icon={StarOutlined} text="156" key="list-vertical-star-o" />,
                                <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
                                <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
                            ]}
                            extra={
                                <video
                                    width={272}
                                    alt="logo"
                                    src={item.src}
                                />
                            }
                        >
                            <List.Item.Meta
                                avatar={<Avatar src={item.avatar} />}
                                title={<a href={item.href}>{item.mentor} - {item.title}</a>}
                                description={item.description}
                            />
                            {item.content}
                        </List.Item>
                    )}
                />
            </div >
        )
    }
}

const IconText = ({ icon, text }) => (
    <Space>
        {React.createElement(icon)}
        {text}
    </Space>
);

export default DiscoverMaterials;