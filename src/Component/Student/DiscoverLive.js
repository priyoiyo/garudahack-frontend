import React from 'react';
import StudentCalendar from './StudentCalendar';

const DiscoverLive = () => {
    return (
        <div className='st-container'>
            <h3>Live Course Schedule</h3>
            <StudentCalendar />
        </div>
    )
}

export default DiscoverLive;