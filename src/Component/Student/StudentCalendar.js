import React from 'react';
import { Calendar, Badge } from 'antd';

function getListData(value) {
    let listData;
    switch (value.date()) {
        case 8:
            listData = [
                { type: 'warning', content: 'Live course via zoom 1.' },
                { type: 'success', content: 'Live course via zoom 2.' },
            ];
            break;
        case 10:
            listData = [
                { type: 'warning', content: 'Live course via zoom 3.' },
                { type: 'success', content: 'Live course via zoom 4.' },
                { type: 'error', content: 'This is error event 5.' },
            ];
            break;
        case 15:
            listData = [
                { type: 'warning', content: 'Live course via zoom 6' },
                { type: 'success', content: 'Live course via zoom 7。。....' },
                { type: 'error', content: 'Live course via zoom 1.' },
                { type: 'error', content: 'Live course via zoom 2.' },
                { type: 'error', content: 'Live course via zoom 3.' },
                { type: 'error', content: 'Live course via zoom 4.' },
            ];
            break;
        default:
    }
    return listData || [];
}

function dateCellRender(value) {
    const listData = getListData(value);
    return (
        <ul className="events">
            {listData.map(item => (
                <li key={item.content}>
                    <Badge status={item.type} text={item.content} />
                </li>
            ))}
        </ul>
    );
}

function getMonthData(value) {
    if (value.month() === 8) {
        return 1394;
    }
}

function monthCellRender(value) {
    const num = getMonthData(value);
    return num ? (
        <div className="notes-month">
            <section>{num}</section>
            <span>Backlog number</span>
        </div>
    ) : null;
}


const StudentCalendar = () => {
    return (
        <div className="site-calendar-demo-card">
           <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
        </div>)
}

export default StudentCalendar;
