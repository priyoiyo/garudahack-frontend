import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LessonDetail from './Pages/Lesson/LessonDetail';
import LessonCheckout from './Pages/Lesson/LessonCheckout';
import StudentPage from './Pages/Student/StudentPage';
import './App.css';
import TeacherPage from './Pages/Teacher/TeacherPage';
import Sidebar from './Component/common/sidebar'
import LandingPage from './Pages/Landing Page'

function App() {
  navigator.geolocation.getCurrentPosition(function(position){
    sessionStorage.setItem('latitude', position.coords.latitude)
    sessionStorage.setItem('longitude', position.coords.longitude)
  })
  return (
    <div className="App">
      <BrowserRouter>
        {/* <Header></Header> */}
        <Sidebar></Sidebar>
        <Switch>
          <Route exact path='/' component={LandingPage}/>
          <Route exact path='/lesson-detail/:id' component={LessonDetail}/>
          <Route exact path='/lesson-checkout' component={LessonCheckout}/>

          <Route path="/student" component={StudentPage} />
          <Route path="/teacher" component={TeacherPage} />


        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
