import * as geolib from 'geolib';

// class DistanceHelper{
//     constructor(lat, long){
//         this.latStart = parseInt(lat);
//         this.longStart= parseInt(long);
//         this.position = navigator.geolocation.getCurrentPosition()
//     }

//     getDistance(){
//         return this.calcDistance()
//     }

//     calcDistance(){
//         return geolib.getDistance(
//             {latitude: this.position.coords.latitude, longitude: this.position.coords.longitude},
//             { latitude: this.lat, longitude: this.long } )
//     }

// }
function DistanceHelper(lat, long) {

    return geolib.getDistance(
        { latitude: sessionStorage.getItem('latitude'), longitude: sessionStorage.getItem('longitude') },
        { latitude: parseInt(lat), longitude: parseInt(long) })

}

export default DistanceHelper

